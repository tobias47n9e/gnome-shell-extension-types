# GNOME Shell Extension Types

Type definitions for creating GNOME shell extensions.

Creating shell extensions for GNOME can be difficult because the JS API is huge and there
is no IDE support for GNOME JS (GJS) and the GNOME Shell API. This package tries to make
it easier to write extensions and find bugs in existing extensions by using a TypeScript
type decoration file.

> Warning: This package is very incomplete and buggy and might give wrong type information.

## Installation

If you don't have a `package.json` yet use `npm init` to create it for your extension.

Then install the package as a development dependency using:

`npm install gnome-shell-extension-types --save-dev`

## Usage

Rename your extension files from `*.js` to `.ts`.

Then add the type decoration file at the top of `extension.ts`:

```typescript
/// <reference path="node_modules/gnome-shell-extension-types/global.d.ts"/>
```

> Depending on your folder structure the path might be different!

If everything works correctly your IDE should now give you type information for the API.
